export default function (Vue, axios) {
  if (!axios) {
    console.error('You have to install axios')
    return
  }

  Vue.axios = axios

  Object.defineProperties(Vue.prototype, {
    axios: {
      get () {
        return axios
      }
    },
    $http: {
      get () {
        return axios
      }
    }
  })
}
