export default function (Vue) {
  Vue.auth = {
    setToken (token, expiration, refresh, type) {
      let access = {
        access: token,
        expiration: expiration,
        refresh: refresh,
        type: type
      }

      localStorage.setItem('token', JSON.stringify(access))
    },
    getToken () {
      let access = JSON.parse(localStorage.getItem('token'))

      // if (!access || !access.expiration) {
      if (!access) {
        return null
      }

       // if (Date.now() > parseInt(access.expiration)) {
       //     this.destroyToken()
       //     return null
       // } else {
      return access
       // }
    },
    destroyToken () {
      localStorage.removeItem('token')
    },
    isAuthenticated () {
      return !!this.getToken()
    }
  }

  Object.defineProperties(Vue.prototype, {
    $auth: {
      get () {
        return Vue.auth
      }
    }
  })
}
