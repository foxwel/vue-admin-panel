import { ApolloClient } from 'apollo-client'
// import { HttpLink } from 'apollo-link-http'
import { InMemoryCache } from 'apollo-cache-inmemory'
import { API_HOST, GRAPH_QUERY } from '@/api'
const { createUploadLink } = require('apollo-upload-client')

const apolloClient = new ApolloClient({
  // link: new HttpLink({
  //   uri: API_HOST + GRAPH_QUERY
  // }),
  link: createUploadLink({
    uri: API_HOST + GRAPH_QUERY
  }),
  cache: new InMemoryCache(),
  connectToDevTools: true
})

export default apolloClient
