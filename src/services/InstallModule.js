import App from '../main'

class InstallModule {
  constructor (module) {
    this._settings = null

    return this.install(module)
  }

  install (module) {
    this.installRouter(module.getStructureRoute())
    this.installSettings(module.getStructureSettings())

    return this._createMap(module)
  }

  installRouter (routes) {
    const routeScheme = App.router.options.routes.map(route => {
      if (route.name === 'AppLayout') {
        Object.assign(route.children, routes)
      }
      return route
    })

    App.router.addRoutes(routeScheme)
  }

  installSettings (settings) {
    this._settings = settings
  }

  _createMap (module) {
    return Object.assign(module._module, {
      settings: this._settings
    })
  }
}

export default function (module) {
  return new InstallModule(module)
}
