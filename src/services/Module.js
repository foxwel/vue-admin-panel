class Module {
  constructor (module, route) {
    this._module = module
    this._route = route
  }

  getRules (routeName) {
    if (this._module.settings) {
      if (routeName && this._module.settings[routeName].rules) {
        return this._module.settings[routeName].rules
      }

      return this._module.settings
    }
  }

  getModule () {
    return this._module
  }
}

export default Module
