import Module from './Module'

function ModuleException (message) {
  this.message = message
  this.name = 'Module Fw'
}

class ModuleDefault extends Module {
  constructor (module, options) {
    super()

    this._module = module
    this._settings = options.settings || null
    this._routerStructure = []
  }

  getModuleInfo () {
    if (!this._module.name) {
      throw new ModuleException('The name is not appropriated to the module')
    }

    return Object.assign({
      name: '',
      title: '',
      description: ''
    }, this._module)
  }

  getStructureSettings () {
    if (this._settings && this._settings.length > 0) {
      let settings = typeof this._settings === 'string' ? JSON.parse(this._settings) : this._settings

      return settings[this.getModuleInfo().name] || null
    }

    return null
  }

  getStructureRoute () {
    const getModuleInfo = this.getModuleInfo()

    this._routerStructure = [{
      path: '/' + getModuleInfo.name,
      component: () => System.import(`@/components/modules/default/Default`),
      children: [{
        path: '',
        name: getModuleInfo.name,
        component: () => System.import(`@/components/modules/default/Home`)
      }, {
        path: 'view',
        name: getModuleInfo.name + '.view',
        component: () => System.import(`@/components/modules/default/View`)
      }, {
        path: 'edit',
        name: getModuleInfo.name + '.edit',
        component: () => System.import(`@/components/modules/default/Edit`)
      }, {
        path: 'create',
        name: getModuleInfo.name + '.create',
        component: () => System.import(`@/components/modules/default/Edit`)
      }, {
        path: 'settings',
        name: getModuleInfo.name + '.settings',
        component: () => System.import(`@/components/modules/default/Settings`)
      }]
    }]

    return this._routerStructure
  }
}

export default ModuleDefault
