class ModuleRule {
  constructor (rule) {
    this._rule = rule
    this._schema = []
    this._schemaId = []
    this._options = []
    this._form = []
    this._viewName = null
  }

  getRule (viewName) {
    this._viewName = viewName

    return this
  }

  getView () {
    for (let rule in this._rule) {
      switch (rule) {
        case 'fetchData':
          return this._getViewFetch(this._rule[rule])

        case 'form':
          return this._getViewForm()
      }
    }
  }

  getSchema () {
    return this._schema[this._viewName]
  }

  getSchemaId () {
    return this._schemaId[this._viewName]
  }

  getOptions () {
    return this._options[this._viewName] || {}
  }

  getForm () {
    return this._form
  }

  _getViewFetch (rule) {
    if (rule) {
      return rule.map(item => {
        this._schema[item.type] = item.schema || null
        this._schemaId[item.type] = item.schemaId || null
        this._options[item.type] = item.options || null

        return item.type
      })
    }
  }

  _getViewForm () {
    let form = this._rule.form

    if (form) {
      this._form = form
      return 'Form'
    }
  }
}

export default ModuleRule
