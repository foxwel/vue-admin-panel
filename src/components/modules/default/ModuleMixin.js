import ModuleRule from '@/services/ModuleRule'
import ListItems from '@/components/views/ListItems'
import Sequence from '@/components/views/Sequence'
import Form from '@/components/views/Form'

export default {
  components: {
    Sequence,
    ListItems,
    Form
  },
  props: ['module'],
  watch: {
    '$route' () {
      this.moduleStack()
    }
  },
  data: () => ({
    routeName: 'main',
    view: null,
    moduleRule: null
  }),
  computed: {
    moduleName () {
      return this.module.getModule().name
    },
    moduleTitle () {
      return this.module.getModule().title
    }
  },
  methods: {
    moduleStack () {
      this.moduleRule = new ModuleRule(this.module.getRules(this.routeName))
      this.view = this.moduleRule.getView()
    }
  },
  created () {
    this.moduleStack()
  }
}
