import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import axios from 'axios'
import Auth from '@/services/Auth'
import VueAxios from '@/services/Axios'

import VueApollo from 'vue-apollo'
import ApolloClient from '@/services/ApolloClient'

import ElementUI from 'element-ui'
import localeElementUI from 'element-ui/lib/locale/lang/ru-RU'
import './styles/ui.scss'

Vue.config.productionTip = false

Vue.use(Auth)
Vue.use(ElementUI, {
  locale: localeElementUI
})
Vue.use(VueAxios, axios)
Vue.use(VueApollo)

Vue.directive('click-outside', {
  bind (el, binding, vnode) {
    el.event = event => {
      if (!(el === event.target || el.contains(event.target))) {
        vnode.context[binding.expression](event)
      }
    }
    document.body.addEventListener('click', el.event)
  },
  unbind: el => document.body.removeEventListener('click', el.event)
})

Vue.axios.defaults.baseURL = 'http://localhost:8000/'
Vue.axios.interceptors.request.use(response => {
  response.headers['Access-Control-Allow-Origin'] = '*'

  if (Vue.auth.isAuthenticated()) {
    response.headers.Authorization = Vue.auth.getToken().type + ' ' + Vue.auth.getToken().access
  }

  return response
}, error => Promise.reject(error))

const apolloProvider = new VueApollo({
  defaultClient: ApolloClient
})

/* eslint-disable no-new */
const app = new Vue({
  el: '#app',
  router,
  store,
  apolloProvider,
  render: h => h(App)
})

export default { router, app }
