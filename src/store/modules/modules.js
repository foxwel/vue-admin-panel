import ModuleDefault from '@/services/ModuleDefault'
import InstallModule from '@/services/InstallModule'
import apolloClient from '@/services/ApolloClient'
import gql from 'graphql-tag'
import * as types from '../mutation-types'

const state = {
  data: []
}

const getters = {
  modules: state => state.data,
  getModule: ({ data: modules }) => {
    return (moduleName) => {
      return modules.hasOwnProperty(moduleName) ? modules[moduleName] : null
    }
  }
}

const actions = {
  receive ({ commit }) {
    apolloClient.query({
      query: gql`
        query {
          modules {
            data {
              name
              title
              description
              icon
            }
          }
          module_settings
        }
      `
    }).then(result => {
      const modulesMap = {}

      result.data.modules.data.forEach(module => {
        modulesMap[module.name] = InstallModule(new ModuleDefault(module, {
          settings: result.data.module_settings
        }))
      })

      commit('SET_MODULES', modulesMap)
    })
  },

  fetch ({ commit }, moduleName) {
    apolloClient.query({
      query: gql`
        query {
          modules {
            data {
              name,
              title,
              description,
              icon
            }
          }
        }
      `
    }).then(result => {
      console.log(result)
    })
  }
}

const mutations = {
  [types.SET_MODULES] (state, data) {
    state.data = data
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
