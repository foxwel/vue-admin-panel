import Vue from 'vue'
import Router from 'vue-router'
import AppLayout from '@/components/AppLayout'
import Home from '@/components/home/Home'
import Settings from '@/components/settings/Settings'
import SettingsGeneral from '@/components/settings/SettingsGeneral'
import SettingsSecurity from '@/components/settings/SettingsSecurity'
import Login from '@/components/auth/Login'
// import Logout from '@/components/auth/Logout'

Vue.use(Router)

const router = new Router({
  routes: [
    {
      path: '/',
      auth: true,
      name: 'AppLayout',
      component: AppLayout,
      meta: { requiresAuth: true },
      children: [
        {
          path: '',
          name: 'home',
          component: Home
        }, {
          path: '/settings',
          component: Settings,
          children: [
            {
              path: '',
              name: 'settings.general',
              component: SettingsGeneral
            }, {
              path: '/security',
              name: 'settings.security',
              component: SettingsSecurity
            }
          ]
        }
      ]
    }, {
      path: '/login',
      name: 'login',
      component: Login
    }, {
      path: '/logout',
      name: 'logout'
      // component: Logout
    }
  ],

  scrollBehavior (to, from, savedPosition) {
    return {
      x: 0,
      y: 0
    }
  }
})

router.beforeEach((to, from, next) => {
  const auth = router.app.$auth

  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!auth.isAuthenticated()) {
      next({
        path: '/login',
        query: { redirect: to.fullPath }
      })
    } else {
      next()
    }
  }

  if (to.name === 'logout') {
    if (auth.isAuthenticated()) {
      auth.destroyToken()
      next({ name: 'login' })
    }
  }

  next()
})

export default router
